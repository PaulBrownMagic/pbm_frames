:- object(tests,
	extends(lgtunit)).

    :- uses(list, [member/2, length/2]).
	:- info([
		version is 1.0,
		author is 'Paul Brown',
		date is 2018/11/06,
		comment is 'Unit tests for frames.'
	]).

	cover(frames).
    cover(frames_interface).
    cover(frame(_,_)).

	test(frame_all_slots) :-
        frames::get(foo, [a1-v1]),
        frames::get(bar, [a1-v1, a2-v2]),
		frames::get(baz, [ako-foo, a3-v2, a4-v3]).

	test(frame_mixed_requirements) :-
        frames::get(foo, a1-v1),
        frames::get(bar, a1-v1),
        frames::get(bar, a2-v2),
        frames::get(baz, [a3-v2, a4-v3]),
        findall(V, frames::get(baz, _-V), Vs),
        ground(Vs), member(v1, Vs),
        member(v2, Vs), member(v3, Vs),
        member(foo, Vs), length(Vs, 4).

    test(frame_names) :-
        findall(N, frames::get(N, a1-v1), Names),
        ground(Names), member(foo, Names),
        member(bar, Names), member(baz, Names),
        length(Names, 3).

    test(frame_object) :-
        {frame(foo, _)}::slots([a1-v1]),
        {frame(_, [a1-v1])}::name(foo),
        {frame(foo, [a1-v1])}::ground.

:- end_object.
