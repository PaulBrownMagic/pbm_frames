:- use_module(library(logtalk)).

:- initialization((
	set_logtalk_flag(report, warnings),
	logtalk_load(lgtunit(loader)),
    logtalk_load(library(basic_types_loader)),
	logtalk_load(frame, [debug(on), source_data(on)]),
    load_frames_file(test_frames),
	logtalk_load(tests, [hook(lgtunit)]),
	tests::run
)).
