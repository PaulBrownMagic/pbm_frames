load_frames_file(File) :-
    consult(File).

:- object(frame(_Name, _Slots)).

    :- public([ name/1
              , slots/1
              , ground/0
              ]).

    name(Name) :-
        parameter(1, Name).

    slots(Slots) :-
        parameter(2, Slots).

    ground.

:- end_object.

:- object(frames_interface).

    :- public(get/2).
    :- protected([ get_slots/2
                 , get_from_slots/2
                 ]).

    get(Name, Slots) :-
        ::get_slots(Name, Slots).
    get(Name, Required) :-
        \+ var(Required), % Should be slots then
        \+ get_slots(Name, Required), % all slots spelled out, resolved before
        ::get_slots(Name, Slots),
        ::get_from_slots(Required, Slots).

    get_slots(_, _) :-
        fail.

:- end_object.

:- object(frames,
    extends(frames_interface)
).
    :- uses(list, [member/2]).
    :- uses(frame(_,_),
        [ name/1
        , slots/1
        ]).

    get_slots(Name, Slots) :-
        {frame(Name, Slots)}::ground.

    %! get_from_slots(?Required, +Slots:list) is nondet.
    %  extract the required attr=value pairs from the provided slots
    get_from_slots([], _).  % Nothing required
    get_from_slots(Attr-Value, Slots) :-  % Singular required in Slots
        member(Attr-Value, Slots),
        \+ is_list(Value).
    get_from_slots(Attr-Value, Slots) :-  % List of values, yield with member
        member(Attr-List, Slots),
        is_list(List),
        member(Value, List).
    get_from_slots(Attr-Value, Slots) :-  % check ako
        member(ako-Ako, Slots),
        ^^get(Ako, Attr-Value).
    get_from_slots(Attr-Value, Slots) :-  % check isa
        member(isa=Ako, Slots),
        ^^get(Ako, Attr-Value).
    get_from_slots([Required|Tail], Slots) :-  % List of Req, recurse.
        get_from_slots(Required, Slots),
        get_from_slots(Tail, Slots).

:- end_object.

/*
:- object(abstract_frame_decorator(_Frame_),
    implements(forwarding),
    extends(frame_interface)
).

   forward(Message) :-
       [_Frame_::Message].

:- end_object.


:- object(mutable_decorator(_Frame_),
    extends(abstract_frame_decorator(_Frame_))
).

    get_slots(Name, Slots) :-
        mframe(Name, Slots).
    get_slots(Name, Slots) :-
        _Frame_::get_slots(Name, Slots).

:- end_object.
*/
